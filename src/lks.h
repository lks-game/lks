#ifndef GDEXAMPLE_H
#define GDEXAMPLE_H

#include <godot_cpp/classes/sprite2d.hpp>

namespace godot {

class LKS : public Sprite2D {
	GDCLASS(LKS, Sprite2D)

private:
	double time_passed;

protected:
	static void _bind_methods();

public:
	LKS();
	~LKS();

	void _process(double delta) override;
};

}

#endif