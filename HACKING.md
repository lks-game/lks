# Setup Dev Environment
This guide is currently only for windows, clion and visual studio compiler.

* Install visual studio community, python scons and git like described in https://docs.godotengine.org/en/stable/contributing/development/compiling/compiling_for_windows.html
* Fetch the code via the command """git clone https://gitlab.com/lks-game/lks; cd lks; git submodule update --init """
* Download latest godot exe file from https://godotengine.org/download/windows/ rename it to Godot.exe and move it to path\to\lks\demo\bin\.
* Generate extension_api.json by running the command """path\to\lks\demo\bin\Godot.exe --dump-extension-api""" and move it to path\to\lks\godot-cpp\.
* Build godot-cpp via the command """cd path\to\lks\godot-cpp ; scons dev_build=yes"""
* Build lks via the command """cd path\to\lks; scons dev_build=yes compiledb=yes"""
* That's it. Now you can open the folder path\to\lks via Clion and you should be able to start hacking.

# Further Literature
* https://docs.godotengine.org/en/stable/tutorials/scripting/gdextension/gdextension_cpp_example.html
* https://docs.godotengine.org/en/stable/contributing/development/configuring_an_ide/clion.html
* https://docs.godotengine.org/en/stable/contributing/development/compiling/compiling_for_windows.html